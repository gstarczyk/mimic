<?php

namespace Gstarczyk\Mimic\IntegrationTest\Mock;

use Gstarczyk\Mimic\IntegrationTest\Fixture\NotExistingClassAsMethodParams;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;

class MethodArgumentFactoryTest extends TestCase
{
    /** @var MethodArgumentFactory */
    private $factory;

    protected function setUp(): void
    {
        $this->factory = new MethodArgumentFactory();
    }

    /**
     * @param ReflectionParameter $parameter
     * @param string $expectedClassName
     *
     * @dataProvider parameterProvider
     */
    public function testCreateObjectArgumentWithNotExistingClass(ReflectionParameter $parameter, string $expectedClassName)
    {
        /** @var ObjectArgumentDefinition $argument */
        $argument = $this->factory->createArgument($parameter);

        Assert::assertInstanceOf(ObjectArgumentDefinition::class, $argument);
        Assert::assertEquals($expectedClassName, $argument->getClassName());
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function parameterProvider()
    {
        $class = new ReflectionClass(NotExistingClassAsMethodParams::class);
        $method1 = $class->getMethod('testMethodFullClassName');
        $method2 = $class->getMethod('testMethodShortClassName');

        return [
            'short class name' => [$method1->getParameters()[0], 'Not\Existing\Klass1'],
            'full class name' => [$method2->getParameters()[0], 'Not\Existing\Klass2'],
        ];
    }
}
