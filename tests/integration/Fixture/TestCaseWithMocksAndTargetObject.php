<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

/**
 * test case with mocks and target object
 */
class TestCaseWithMocksAndTargetObject
{
    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\InterfaceToMock
     * @mock
     */
    public $mock1;

    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\Mock2
     * @mock
     */
    public $mock2;

    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\Mock1
     * @mock
     */
    public $mock3;

    /**
     * @var \Gstarczyk\Mimic\IntegrationTest\Fixture\TargetObject
     * @injectMocks
     */
    public $object;
}