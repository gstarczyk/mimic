<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class InvalidTargetObject
{
    public $prop1;

    public function __construct($prop1)
    {
        $this->prop1 = $prop1;
    }
}