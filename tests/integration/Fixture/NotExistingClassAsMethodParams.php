<?php

namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

use Not\Existing\Klass2;

class NotExistingClassAsMethodParams
{
    public function testMethodFullClassName(\Not\Existing\Klass1 $arg1)
    {

    }

    public function testMethodShortClassName(Klass2 $arg1)
    {

    }
}