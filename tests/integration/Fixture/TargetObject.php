<?php
namespace Gstarczyk\Mimic\IntegrationTest\Fixture;

class TargetObject
{
    /** @var InterfaceToMock */
    public $mock1;

    /** @var Mock2 */
    public $mock2;

    /** @var Mock1 */
    public $mock3;

    public function __construct(InterfaceToMock $mock1, Mock2 $mock2, Mock1 $mock3)
    {
        $this->mock1 = $mock1;
        $this->mock2 = $mock2;
        $this->mock3 = $mock3;
    }
}