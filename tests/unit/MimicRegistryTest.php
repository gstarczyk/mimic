<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\MimicRegistry;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use stdClass;

class MimicRegistryTest extends TestCase
{
    /** @var MimicRegistry */
    private $mimicRegistry;

    /** @var MockFactory | MockObject */
    private $mockFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->mockFactory = $this->getMockBuilder(MockFactory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->mimicRegistry = new MimicRegistry($this->mockFactory);
    }

    public function testRegistryThrowExceptionWhenNoStubIsFound()
    {
        $this->expectException(MimicException::class);
        $this->mimicRegistry->getStub(new stdClass());
    }

    public function testRegistryThrowExceptionWhenNoInvocationRegistryIsFound()
    {
        $this->expectException(MimicException::class);
        $this->mimicRegistry->getInvocationRegistry(new stdClass());
    }

    public function testGetMockCreatesNewMockAndRegisterStubAndInvocationRegistry()
    {
        $this->mockFactory->method('createMock')
            ->willReturn(new stdClass());

        $mock = $this->mimicRegistry->getMock('stdClass');

        $stub = $this->mimicRegistry->getStub($mock);
        $invocationRegistry = $this->mimicRegistry->getInvocationRegistry($mock);

        $this->assertInstanceOf(Stub::class, $stub);
        $this->assertInstanceOf(InvocationRegistry::class, $invocationRegistry);
    }
}
