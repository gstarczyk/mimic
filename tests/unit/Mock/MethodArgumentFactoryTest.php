<?php

namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\ArrayArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\BuiltInArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use ReflectionNamedType;
use ReflectionParameter;

class MethodArgumentFactoryTest extends TestCase
{
    /** @var MethodArgumentFactory */
    private $factory;

    protected function setUp(): void
    {
        $this->factory = new MethodArgumentFactory();
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForObjectType()
    {
        $reflection = $this->createReflectionParameter(
            [
                'getClass' => 'MyClass',
            ]
        );
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ObjectArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForArrayType()
    {
        $reflection = $this->createReflectionParameter(
            [
                'isArray' => true,
            ]
        );
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ArrayArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForMixedType()
    {
        $reflection = $this->createReflectionParameter([]);
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(VariantArgumentDefinition::class, $result);
        Assert::assertFalse($result->isOptional());
        Assert::assertFalse($result->isPassedByReference());
    }

    /**
     * @param ReflectionNamedType $type
     * @throws ReflectionException
     * @dataProvider builtInTypeProvider
     */
    public function testCreateDefinitionForBuiltInType(ReflectionNamedType $type)
    {
        $reflection = $this->createReflectionParameter(
            [
                'getType' => $type,
            ]
        );
        /** @var BuiltInArgumentDefinition $result */
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(BuiltInArgumentDefinition::class, $result);
        Assert::assertEquals($result->getType(), $type->getName());
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function builtInTypeProvider()
    {
        return [
            'string' => [$this->createReflectionType(['getName' => 'string'])],
            'int' => [$this->createReflectionType(['getName' => 'int'])],
            'bool' => [$this->createReflectionType(['getName' => 'bool'])],
            'float' => [$this->createReflectionType(['getName' => 'float'])],
            'object' => [$this->createReflectionType(['getName' => 'object'])],
            'iterable' => [$this->createReflectionType(['getName' => 'iterable'])],
        ];
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForArrayTypeWithDefaultValue()
    {
        $reflection = $this->createReflectionParameter(
            [
                'isArray' => true,
                'isDefaultValueAvailable' => true,
                'getDefaultValue' => ['item'],
            ]
        );
        /** @var ArrayArgumentDefinition $result */
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(ArrayArgumentDefinition::class, $result);
        Assert::assertEquals(['item'], $result->getDefaultValue());
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForMixedTypeWithDefaultValue()
    {
        $reflection = $this->createReflectionParameter(
            [
                'isDefaultValueAvailable' => true,
                'getDefaultValue' => 'myDefaultValue',
            ]
        );
        /** @var VariantArgumentDefinition $result */
        $result = $this->factory->createArgument($reflection);

        Assert::assertInstanceOf(VariantArgumentDefinition::class, $result);
        Assert::assertEquals('myDefaultValue', $result->getDefaultValue());
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForOptionalParameter()
    {
        $reflection = $this->createReflectionParameter(
            [
                'isOptional' => true,
            ]
        );
        $result = $this->factory->createArgument($reflection);

        Assert::assertTrue($result->isOptional());
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateDefinitionForParameterPassedByReference()
    {
        $reflection = $this->createReflectionParameter(
            [
                'isPassedByReference' => true,
            ]
        );
        $result = $this->factory->createArgument($reflection);

        Assert::assertTrue($result->isPassedByReference());
    }

    /**
     * @param array $data
     *
     * @return MockObject | ReflectionParameter
     * @throws ReflectionException
     */
    private function createReflectionParameter(array $data)
    {
        $defaultData = [
            'getName' => 'param1',
            'getClass' => null,
            'isArray' => false,
            'isDefaultValueAvailable' => false,
            'getDefaultValue' => null,
            'isOptional' => false,
            'isPassedByReference' => false,
        ];
        if (isset($data['getClass'])) {
            $classReflection = $this->createMock(ReflectionClass::class);
            $classReflection->expects($this->any())
                ->method('getName')
                ->willReturn($data['getClass']);
            $data['getClass'] = $classReflection;
        }

        $mergedData = array_merge($defaultData, $data);
        $reflection = $this->createMock(ReflectionParameter::class);

        foreach ($mergedData as $name => $value) {
            $reflection->expects($this->any())
                ->method($name)
                ->willReturn($value);
        }

        return $reflection;
    }

    /**
     * @param array $data
     * @return MockObject | ReflectionNamedType
     * @throws ReflectionException
     */
    private function createReflectionType(array $data): ReflectionNamedType
    {
        $defaults = [
            'getName' => null,
            'allowsNull' => false,
            'isBuiltin' => true,
        ];
        $mergedData = array_merge($defaults, $data);
        $mockObject = $this->createMock(ReflectionNamedType::class);
        foreach ($mergedData as $name => $value) {
            $mockObject->expects($this->any())
                ->method($name)
                ->willReturn($value);
        }

        return $mockObject;
    }
}
