<?php

namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MethodDefinition;
use Gstarczyk\Mimic\Mock\MethodDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodReturnTypeFactory;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassWithFinalMethod;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionMethod;

class MethodDefinitionFactoryTest extends TestCase
{
    /**
     * @var MethodDefinitionFactory
     */
    private $factory;

    /**
     * @var MethodArgumentFactory | MockObject
     */
    private $argumentFactory;

    /**
     * @var MethodReturnTypeFactory | MockObject
     */
    private $returnTypeFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->argumentFactory = $this->createMock(MethodArgumentFactory::class);
        $this->returnTypeFactory = $this->createMock(MethodReturnTypeFactory::class);
        $this->factory = new MethodDefinitionFactory(
            $this->argumentFactory,
            $this->returnTypeFactory
        );
    }

    public function testCreateDefinitionOfFinalMethodWillThrowException(): void
    {
        // then
        $this->expectException(MimicException::class);

        // given
        $method = new ReflectionMethod(ClassWithFinalMethod::class, 'finalMethod');

        // when
        $this->factory->createMethodDefinition($method);
    }

    public function testFactoryProduceMethodDefinition(): void
    {
        // given
        $method = new ReflectionMethod(ClassOne::class, 'publicMethodWithoutArgumentsAndReturnType');
        $this->argumentsFactoryWillReturn([]);

        // when
        $result = $this->factory->createMethodDefinition($method);

        // then
        $expected = new MethodDefinition();
        $expected->setMethodName('publicMethodWithoutArgumentsAndReturnType');
        $expected->setInstructions([$this->factory::INVOCATION_HANDLER_WITH_RETURN]);

        Assert::assertEquals($expected, $result);
    }

    private function argumentsFactoryWillReturn(array $list): void
    {
        $this->argumentFactory
            ->method('createArgumentsList')
            ->willReturn($list);
    }
}
