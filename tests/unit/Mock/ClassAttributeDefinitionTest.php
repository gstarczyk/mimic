<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\ClassAttributeDefinition;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ClassAttributeDefinitionTest extends TestCase
{
    public function testToCodeProduceCodeDefinition()
    {
        $attribute = new ClassAttributeDefinition('attributeName');
        $expected = 'private $attributeName;';

        Assert::assertEquals($expected, $attribute->toCode());

    }
}
