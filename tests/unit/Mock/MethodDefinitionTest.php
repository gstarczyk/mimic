<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\VariantArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodDefinition;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class MethodDefinitionTest extends TestCase
{
    /** @var  VariantArgumentDefinition | MockObject */
    private $arg2;

    /** @var  VariantArgumentDefinition | MockObject */
    private $arg1;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->arg1 = $this->getMockBuilder(VariantArgumentDefinition::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->arg2 = $this->getMockBuilder(VariantArgumentDefinition::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testToCodeProduceTextRepresentation()
    {
        $this->arg1->expects($this->any())->method('toCode')->willReturn('$arg1');
        $this->arg2->expects($this->any())->method('toCode')->willReturn('$arg2');

        $definition = new MethodDefinition();
        $definition->setMethodName('someMethod');
        $definition->setInstructions([
            'instruction 1',
            'instruction 2',
            'instruction 3',
        ]);
        $definition->setArguments([
            $this->arg1,
            $this->arg2,
        ]);

        $expected = 'public function someMethod($arg1, $arg2)' . PHP_EOL
            . '{' . PHP_EOL
            . 'instruction 1;' . PHP_EOL
            . 'instruction 2;' . PHP_EOL
            . 'instruction 3;' . PHP_EOL
            . '}';

        Assert::assertEquals($expected, $definition->toCode());
    }

    public function testToCodeProduceTextRepresentationForPHP7()
    {
        $this->arg1->expects($this->any())->method('toCode')->willReturn('$arg1');
        $this->arg2->expects($this->any())->method('toCode')->willReturn('$arg2');

        $definition = new MethodDefinition();
        $definition->setMethodName('someMethod');
        $definition->setInstructions([
            'instruction 1',
            'instruction 2',
            'instruction 3',
        ]);
        $definition->setArguments([
            $this->arg1,
            $this->arg2,
        ]);
        $definition->setReturnType('string');

        $expected = 'public function someMethod($arg1, $arg2): string' . PHP_EOL
            . '{' . PHP_EOL
            . 'instruction 1;' . PHP_EOL
            . 'instruction 2;' . PHP_EOL
            . 'instruction 3;' . PHP_EOL
            . '}';

        Assert::assertEquals($expected, $definition->toCode());
    }

    public function testToCodeProduceTextRepresentationForPHP71()
    {
        $this->arg1->expects($this->any())->method('toCode')->willReturn('$arg1');
        $this->arg2->expects($this->any())->method('toCode')->willReturn('$arg2');

        $definition = new MethodDefinition();
        $definition->setMethodName('someMethod');
        $definition->setInstructions([
            'instruction 1',
            'instruction 2',
            'instruction 3',
        ]);
        $definition->setArguments([
            $this->arg1,
            $this->arg2,
        ]);
        $definition->setReturnType('?string');

        $expected = 'public function someMethod($arg1, $arg2): ?string' . PHP_EOL
            . '{' . PHP_EOL
            . 'instruction 1;' . PHP_EOL
            . 'instruction 2;' . PHP_EOL
            . 'instruction 3;' . PHP_EOL
            . '}';

        Assert::assertEquals($expected, $definition->toCode());
    }
}
