<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\InvocationHandler;
use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\Mock\ClassAttributeDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinition;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgument\ObjectArgumentDefinition;
use Gstarczyk\Mimic\Mock\MethodDefinitionFactory;
use Gstarczyk\Mimic\UniqueIdGenerator;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassOne;
use Gstarczyk\Mimic\UnitTest\Fixture\ClassWithFinalMethod;
use Gstarczyk\Mimic\UnitTest\Fixture\FinalClass;
use Gstarczyk\Mimic\UnitTest\Fixture\StaticMethods;
use Gstarczyk\Mimic\UnitTest\Fixture\TestInterface;
use InvalidArgumentException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class ClassDefinitionFactoryTest extends TestCase
{
    /** @var ClassDefinitionFactory */
    private $factory;

    /** @var  UniqueIdGenerator | MockObject */
    private $uniqueIdGeneratorMock;

    /** @var MethodDefinitionFactory | MockObject */
    private $methodDefinitionFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->uniqueIdGeneratorMock = $this->createMock(UniqueIdGenerator::class);
        $this->methodDefinitionFactory = $this->createMock(MethodDefinitionFactory::class);
        $this->factory = new ClassDefinitionFactory(
            $this->uniqueIdGeneratorMock,
            $this->methodDefinitionFactory
        );
    }

    public function testFactoryThrowExceptionWhenGivenClassHasFinalMethod()
    {
        // then
        $this->expectException(MimicException::class);

        // given
        $this->methodDefinitionFactory->expects(self::any())
            ->method('createMethodDefinition')
            ->willThrowException(new InvalidArgumentException());

        // when
        $this->factory->createClassDefinition(ClassWithFinalMethod::class);
    }

    public function testFactoryThrowExceptionWhenGivenFinalClass()
    {
        $this->expectException(MimicException::class);

        $definition = $this->factory->createClassDefinition(FinalClass::class);
        Assert::assertInstanceOf(ClassDefinition::class, $definition);
    }

    public function testFactoryThrowExceptionWhenGivenClassNameIsNotExistingClassOrInterface()
    {
        $this->expectException(MimicException::class);

        $this->factory->createClassDefinition('NotExistingClass');
    }

    public function testFactoryCreatesClassThatExtendsMockedClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('UniqueSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals(ClassOne::class, $definition->getExtends());
    }

    public function testFactoryCreatesClassWithUniqueClassName()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('UniqueSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals('ClassOne_UniqueSuffix', $definition->getShortName());
    }

    public function testFactoryCreatesClassInSameNamespaceAsMockedClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('SomeSuffix');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertEquals('Gstarczyk\Mimic\UnitTest\Fixture', $definition->getNamespace());
    }

    public function testFactoryCreateClassThatOverwriteOnlyPublicMethods()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
        $definition = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertCount(6, $definition->getMethodDefinitions());
    }

    public function testFactoryAlwaysCreateNewClass()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturnOnConsecutiveCalls('Mock1', 'Mock2');
        $definition1 = $this->factory->createClassDefinition(ClassOne::class);
        $definition2 = $this->factory->createClassDefinition(ClassOne::class);

        Assert::assertNotEquals($definition1->getShortName(), $definition2->getShortName());
    }

    public function testFactoryCanMockInterfaces()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');
        $definition = $this->factory->createClassDefinition(TestInterface::class);

        $this->assertNull($definition->getExtends());
        $this->assertEquals(TestInterface::class, $definition->getImplements());
    }

    public function testCreatedClassRequireInvocationHandler()
    {
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');

        $definition = $this->factory->createClassDefinition(TestInterface::class);

        $this->assertClassRequireInvocationHandler($definition);

        Assert::assertTrue(true); // only to make assertion visible by phpunit
    }

    public function testStaticMethodsDoNotBreakMocking()
    {
        $this->expectException(MimicException::class);
        $this->uniqueIdGeneratorMock->method('generateId')->willReturn('Mock');

        $definition = $this->factory->createClassDefinition(StaticMethods::class);

        $definition->getMethodDefinition('staticMethod');
    }

    private function assertClassRequireInvocationHandler(ClassDefinition $definition)
    {
        $attribute = new ClassAttributeDefinition('__invocationHandler');
        $argument = new ObjectArgumentDefinition('invocationHandler', InvocationHandler::class);
        $constructor = $definition->getMethodDefinition('__construct');

        Assert::assertNotNull($constructor);
        Assert::assertContainsEquals($attribute, $definition->getAttributes());
        Assert::assertContainsEquals($argument, $constructor->getArguments());
    }
}
