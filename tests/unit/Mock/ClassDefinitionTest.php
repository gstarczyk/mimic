<?php
namespace Gstarczyk\Mimic\UnitTest\Mock;

use Gstarczyk\Mimic\MimicException;
use Gstarczyk\Mimic\Mock\ClassDefinition;
use PHPUnit\Framework\TestCase;

class ClassDefinitionTest extends TestCase
{
    public function testGetMethodDefinitionThrowExceptionIfDefinitionForGivenNameIsNotFound()
    {
        $this->expectException(MimicException::class);

        $definition = new ClassDefinition();
        $definition->getMethodDefinition('someMethodName');
    }

    public function testCodeGenerationForSimpleClass()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }

    public function testCodeGenerationForClassExtendingOtherClass()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');
        $definition->setExtends('MyOtherClass');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass extends \MyOtherClass'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }

    public function testCodeGenerationForClassImplementsInterface()
    {
        $definition = new ClassDefinition();
        $definition->setShortName('MyNewClass');
        $definition->setNamespace('MyNamespace');
        $definition->setImplements('MyInterface');

        $expected = 'namespace MyNamespace;'.PHP_EOL
            .'class MyNewClass implements \MyInterface'.PHP_EOL
            .'{'.PHP_EOL
            .'}';

        $result = $definition->toCode();

        $this->assertEquals($expected, $result);
    }
}
