<?php

namespace Gstarczyk\Mimic\UnitTest\VerifierBuilder\FilteredInvocations;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\TimesVerifier;
use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;
use Gstarczyk\Mimic\VerifierBuilder\FilteredInvocations\VerifierBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class VerifierBuilderTest extends TestCase
{
    /** @var VerifierBuilder */
    private $builder;

    /** @var InvocationRegistry | MockObject */
    private $invocationRegistryMock;

    /** @var ArgumentsMatcherFactory | MockObject */
    private $argumentsMatcherFactory;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->invocationRegistryMock = $this->createMock(InvocationRegistry::class);
        $this->argumentsMatcherFactory = $this->createMock(ArgumentsMatcherFactory::class);
        $this->builder = new VerifierBuilder($this->invocationRegistryMock, $this->argumentsMatcherFactory);

        $this->invocationRegistryMock
            ->method('invocationCount')
            ->willReturn(10);

        $argumentsMatcher = $this->createMock(ArgumentsMatcher::class);
        $this->argumentsMatcherFactory
            ->method('createMatcher')
            ->withAnyParameters()
            ->willReturn($argumentsMatcher);
    }

    /**
     * @throws ReflectionException
     */
    public function testVerifyThrowsExceptionWhenExpectedTimesWasNotMet()
    {
        $this->expectException(TimesVerificationException::class);

        $this->builder
            ->method('methodOne')
            ->withAnyArguments()
            ->wasCalled($this->createFailingTimesVerifierMock());
    }

    /**
     * @throws ReflectionException
     */
    public function testVerifyExceptionHasMessageWithInvocationDescription()
    {
        $timesVerifierErrorMessage = 'Times verifier error message';
        $methodName = 'someMethod';
        $timesVerifier = $this->createFailingTimesVerifierMock($timesVerifierErrorMessage);

        $this->expectException(TimesVerificationException::class);
        $this->expectExceptionMessage(
            sprintf('Unmet expected invocation count of method "%s". %s', $methodName, $timesVerifierErrorMessage)

        );

        $this->builder
            ->method($methodName)
            ->withAnyArguments()
            ->wasCalled($timesVerifier);
    }

    public function testVerifyUseArgumentsMatcherFactory()
    {
        $this->argumentsMatcherFactory->expects($this->exactly(3))
            ->method('createMatcher')
            ->withConsecutive(
                [['aa']],
                [null],
                [[]]
            );

        $this->builder->with('aa');
        $this->builder->withAnyArguments();
        $this->builder->withoutArguments();
    }

    /**
     * @param string $errorMessage
     * @return TimesVerifier | MockObject
     * @throws ReflectionException
     */
    private function createFailingTimesVerifierMock(string $errorMessage = ''): TimesVerifier
    {
        $timesVerifierMock = $this->createMock(TimesVerifier::class);
        $timesVerifierMock
            ->method('verify')
            ->willThrowException(new TimesVerificationException($errorMessage));

        return $timesVerifierMock;
    }
}
