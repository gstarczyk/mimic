<?php
namespace Gstarczyk\Mimic\UnitTest\Fixture\InitMocks;

/**
 * test case with single mock
 */
class TestCaseOne
{
    /**
     * @var \Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\TestClassOne
     * @mock
     */
    public $mockedObject;
}