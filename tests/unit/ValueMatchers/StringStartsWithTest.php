<?php
namespace Gstarczyk\Mimic\UnitTest\ValueMatchers;

use Gstarczyk\Mimic\ValueMatchers\StringStartsWith;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class StringStartsWithTest extends TestCase
{
    public function testMatcherReturnTrueWhenGivenValueStartsWithSpecifiedString()
    {
        $specification = 'foo';
        $value = 'fooBar';
        $matcher = new StringStartsWith($specification);
        $result = $matcher->match($value);

        Assert::assertTrue($result);
    }

    public function testMatcherReturnFalseWhenGivenValueIsNotStartsWithSpecifiedString()
    {
        $specification = 'foo';
        $value = 'barFoo';
        $matcher = new StringStartsWith($specification);
        $result = $matcher->match($value);

        Assert::assertFalse($result);
    }
}
