<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MockInitiator\Context;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\TypeResolver;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionProperty;
use stdClass;

class ObjectPropertyFactoryTest extends TestCase
{
    /** @var  ObjectPropertyFactory */
    private $factory;

    /** @var TypeResolver | MockObject */
    private $typeResolver;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->typeResolver = $this->createMock(TypeResolver::class);
        $this->factory = new ObjectPropertyFactory(
            $this->typeResolver
        );
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryReturnObjectPropertyInstance()
    {
        $reflection = $this->createReflectionPropertyMock('');
        $context = new Context(new stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertInstanceOf(ObjectProperty::class, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryCanHandlePropertiesWithoutDocBlock()
    {
        $reflection = $this->createReflectionPropertyMock(false);
        $context = new Context(new stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertInstanceOf(ObjectProperty::class, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryUseTypeResolverToSetProperType()
    {
        $this->typeResolver->expects($this->once())
            ->method('resolveType');

        $reflection = $this->createReflectionPropertyMock(
            '/** @var string */'
        );
        $context = new Context(new stdClass(), '', []);

        $this->factory->createObjectProperty($reflection, $context);
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryMarkMockProperties()
    {
        $reflection = $this->createReflectionPropertyMock(
            '/** @mock */'
        );
        $context = new Context(new stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertTrue($result->isMarkedAsMock());
    }

    /**
     * @throws ReflectionException
     */
    public function testFactoryMarkMocksTargetProperties()
    {
        $reflection = $this->createReflectionPropertyMock(
            '/** @injectMocks */'
        );
        $context = new Context(new stdClass(), '', []);

        $result = $this->factory->createObjectProperty($reflection, $context);

        Assert::assertTrue($result->isMarkedAsMocksTarget());
    }

    /**
     * @param string|bool $docComment
     * @return MockObject|ReflectionProperty
     * @throws ReflectionException
     */
    private function createReflectionPropertyMock($docComment)
    {
        $reflection = $this->createMock(ReflectionProperty::class);
        $reflection->method('getDocComment')
            ->willReturn($docComment);

        return $reflection;
    }
}
