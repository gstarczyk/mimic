<?php
namespace Gstarczyk\Mimic\UnitTest\MockInitiator;

use Gstarczyk\Mimic\MimicRegistry;
use Gstarczyk\Mimic\MockInitiator\MockInitiator;
use Gstarczyk\Mimic\MockInitiator\ObjectProperty;
use Gstarczyk\Mimic\MockInitiator\PropertyExtractor;
use Gstarczyk\Mimic\MockInitiator\TargetObjectFactory;
use Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\Mock1;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class MockInitiatorTest extends TestCase
{
    /**  @var MockInitiator */
    private $initiator;

    /** @var MimicRegistry | MockObject */
    private $registry;

    /** @var TargetObjectFactory | MockObject */
    private $targetObjectFactory;

    /** @var PropertyExtractor | MockObject */
    private $propertyExtractor;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->registry = $this->createMock(MimicRegistry::class);
        $this->propertyExtractor = $this->createMock(PropertyExtractor::class);
        $this->targetObjectFactory = $this->createMock(TargetObjectFactory::class);

        $this->initiator = new MockInitiator(
            $this->registry,
            $this->propertyExtractor,
            $this->targetObjectFactory
        );
    }

    /**
     * @throws ReflectionException
     */
    public function testInitMocksCreateMocksForPropertiesMarkedAsMock()
    {
        $testCase = new \stdClass();
        $property = $this->createObjectPropertyMock([
            'marker' => ObjectProperty::MARKER_MOCK,
            'type' => 'Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\Mock1'
        ]);
        $this->makePropertiesExtractorReturn([$property]);

        $mock1 = new Mock1();
        $this->makeRegistryReturn($mock1);

        $property->expects($this->once())
            ->method('setValue')
            ->with($mock1);

        $this->initiator->initMocks($testCase);
    }

    /**
     * @throws ReflectionException
     */
    public function testInitMocksCreateObjectsForPropertiesMarkedAsTarget()
    {
        $testCase = new \stdClass();
        $property = $this->createObjectPropertyMock([
            'type' => '\Gstarczyk\Mimic\UnitTest\Fixture\InitMocks\Mock1',
            'marker' => ObjectProperty::MARKER_TARGET,
        ]);
        $this->makePropertiesExtractorReturn([$property]);

        $targetObject = new Mock1();
        $this->targetObjectFactory->expects($this->any())
            ->method('createTargetObject')
            ->willReturn($targetObject);

        $property->expects($this->once())
            ->method('setValue')
            ->with($targetObject);

        $this->initiator->initMocks($testCase);
    }

    /**
     * @param array $data
     * @return ObjectProperty | MockObject
     * @throws ReflectionException
     */
    private function createObjectPropertyMock(array $data)
    {
        $property = $this->createMock(ObjectProperty::class);
        if (isset($data['type'])) {
            $property
                ->method('getType')
                ->willReturn($data['type']);
        }
        if (isset($data['marker']) && $data['marker'] === ObjectProperty::MARKER_MOCK) {
            $property
                ->method('isMarkedAsMock')
                ->willReturn(true);
        }
        if (isset($data['marker']) && $data['marker'] === ObjectProperty::MARKER_TARGET) {
            $property
                ->method('isMarkedAsMocksTarget')
                ->willReturn(true);
        }

        return $property;
    }

    /**
     * @param array $properties
     */
    private function makePropertiesExtractorReturn(array $properties)
    {
        $this->propertyExtractor
            ->method('extractProperties')
            ->willReturn($properties);
    }

    /**
     * @param object $mock1
     */
    private function makeRegistryReturn($mock1)
    {
        $this->registry
            ->method('getMock')
            ->willReturn($mock1);
    }
}

