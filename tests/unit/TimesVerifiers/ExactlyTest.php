<?php
namespace Gstarczyk\Mimic\UnitTest\TimesVerifiers;

use Gstarczyk\Mimic\TimesVerifiers\Exactly;
use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;
use PHPUnit\Framework\TestCase;

class ExactlyTest extends TestCase
{
    public function testMatcherThrowExceptionWhenExpectationDoesNotMet()
    {
        $this->expectException(TimesVerificationException::class);
        $this->expectExceptionMessage('Expected exactly 2, but 1 was given.');

        $verifier = new Exactly(2);
        $verifier->verify(1);
    }
}
