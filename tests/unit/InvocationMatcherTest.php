<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\InvocationSignature;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class InvocationMatcherTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testMatchReturnTrueOnlyForMatchingInvocations()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(true);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodOne', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertTrue($result);
    }

    /**
     * @throws ReflectionException
     */
    public function testMatchReturnFalseWhenGivenMethodNameIsNotMatched()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(true);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodTwo', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertFalse($result);
    }

    /**
     * @throws ReflectionException
     */
    public function testMatchReturnFalseWhenGivenArgumentsMatcherReturnFalse()
    {
        $argumentsMatcher = $this->createArgumentsMatcher(false);
        $invocation = new InvocationSignature('methodOne', []);
        $matcher = new InvocationMatcher('methodOne', $argumentsMatcher);
        $result = $matcher->match($invocation);

        $this->assertFalse($result);
    }

    /**
     * @param bool $result
     * @return ArgumentsMatcher | MockObject
     * @throws ReflectionException
     */
    private function createArgumentsMatcher($result)
    {
        $argumentsMatcher = $this->createMock(ArgumentsMatcher::class);
        $argumentsMatcher->method('match')->willReturn($result);

        return $argumentsMatcher;
    }
}
