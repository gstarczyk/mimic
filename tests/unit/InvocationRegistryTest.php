<?php
namespace Gstarczyk\Mimic\UnitTest;

use Gstarczyk\Mimic\InvocationMatcher;
use Gstarczyk\Mimic\InvocationRegistry;
use Gstarczyk\Mimic\InvocationSignature;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

class InvocationRegistryTest extends TestCase
{
    /** @var  InvocationRegistry */
    private $registry;

    protected function setUp(): void
    {
        $this->registry = new InvocationRegistry();
    }

    /**
     * @throws ReflectionException
     */
    public function testOnlyMatchingInvocationAreCount()
    {
        $invocations = [
            new InvocationSignature('methodOne', []),
            new InvocationSignature('methodTwo', []),
            new InvocationSignature('methodOne', ['arg1', 20]),
            new InvocationSignature('methodOne', []),
        ];
        $this->prepareInvocations($invocations);
        $matcher = $this->createMatcher([
            [$invocations[0], true],
            [$invocations[1], true],
            [$invocations[2], false],
            [$invocations[3], true],
        ]);

        Assert::assertEquals(3, $this->registry->invocationCount($matcher));
    }

    /**
     * @param array $map
     * @return MockObject | InvocationMatcher
     * @throws ReflectionException
     */
    private function createMatcher(array $map)
    {
        $matcher = $this->getMockBuilder(InvocationMatcher::class)
            ->disableOriginalConstructor()
            ->getMock();
        $matcher->expects(self::atLeastOnce())
            ->method('match')
            ->willReturnMap($map);

        return $matcher;
    }

    private function prepareInvocations(array $invocations)
    {
        foreach ($invocations as $invocation) {
            $this->registry->registerInvocation($invocation);
        }
    }
}
