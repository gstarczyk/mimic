<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsCaptor;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ArgumentsCaptorTest extends TestCase
{
    /** @var ArgumentsCaptor */
    private $matcher;

    protected function setUp(): void
    {
        $this->matcher = new ArgumentsCaptor();
    }

    public function testMatcherGatherAllArguments()
    {
        $this->matcher->match([]);
        $this->matcher->match(['a']);
        $this->matcher->match([2,3]);

        $result = $this->matcher->getValues();

        $expectedResult = [
            [],
            ['a'],
            [2,3]
        ];
        Assert::assertEquals($expectedResult, $result);
    }

}
