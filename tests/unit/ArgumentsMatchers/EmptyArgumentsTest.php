<?php
namespace Gstarczyk\Mimic\UnitTest\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatchers\EmptyArguments;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class EmptyArgumentsTest extends TestCase
{
    public function testMatcherReturnTrueForEmptyArguments()
    {
        $matcher = new EmptyArguments();

        $result = $matcher->match([]);

        Assert::assertTrue($result);
    }
}
