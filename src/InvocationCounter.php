<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

interface InvocationCounter
{
    /**
     * @param InvocationMatcher $invocationMatcher
     * @return int
     */
    public function invocationCount(InvocationMatcher $invocationMatcher): int;
}
