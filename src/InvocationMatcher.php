<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

class InvocationMatcher
{
    /** @var string */
    private $methodName;

    /** @var ArgumentsMatcher */
    private $argumentsMatcher;

    public function __construct(string $methodName, ArgumentsMatcher $argumentsMatcher)
    {
        $this->methodName = $methodName;
        $this->argumentsMatcher = $argumentsMatcher;
    }

    /**
     * @param InvocationSignature $invocationSignature
     * @return bool
     */
    public function match(InvocationSignature $invocationSignature): bool
    {
        $match = false;
        if ($this->methodName == $invocationSignature->getMethodName()) {
            $match = $this->argumentsMatcher->match($invocationSignature->getArguments());
        }

        return $match;
    }
}
