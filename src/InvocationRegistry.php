<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

class InvocationRegistry implements InvocationCounter
{
    /** @var InvocationSignature[] */
    private $invocations = [];

    public function registerInvocation(InvocationSignature $invocationSignature): void
    {
        $this->invocations[] = $invocationSignature;
    }

    /**
     * @param string $methodName
     * @return InvocationSignature[]
     */
    public function getMethodInvocations(string $methodName): iterable
    {
        $result = [];
        foreach ($this->invocations as $invocation) {
            if ($invocation->getMethodName() == $methodName) {
                $result[] = $invocation;
            }
        }

        return $result;
    }

    public function invocationCount(InvocationMatcher $invocationMatcher): int
    {
        $count = 0;
        foreach ($this->invocations as $invocation) {
            if ($invocationMatcher->match($invocation)) {
                ++$count;
            }
        }

        return $count;
    }
}
