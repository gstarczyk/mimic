<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\InvocationHandler;

class MockFactory
{
    /** @var  ClassDefinitionFactory */
    private $definitionFactory;

    public function __construct(ClassDefinitionFactory $definitionFactory)
    {
        $this->definitionFactory = $definitionFactory;
    }

    /**
     * @param string $className
     * @param InvocationHandler $invocationHandler
     * @return object
     */
    public function createMock(string $className, InvocationHandler $invocationHandler): object
    {
        $definition = $this->definitionFactory->createClassDefinition($className);
        $this->loadClass($definition);
        $fullClassName = $definition->getClassName();

        return new $fullClassName($invocationHandler);
    }

    private function loadClass(ClassDefinition $definition): void
    {
        if (!class_exists($definition->getClassName(), false)) {
            eval($definition->toCode());
        }
    }
}
