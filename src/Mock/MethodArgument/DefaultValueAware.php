<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock\MethodArgument;

interface DefaultValueAware
{
    /**
     * @param mixed $defaultValue
     * @return void
     */
    public function setDefaultValue($defaultValue): void;

    /**
     * @return mixed
     */
    public function getDefaultValue();
}
