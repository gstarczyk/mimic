<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock\MethodArgument;

abstract class ArgumentDefinition
{
    /** @var  string */
    private $name;

    /** @var bool */
    private $optional = false;

    /** @var bool */
    private $allowNull = false;

    /** @var bool */
    private $passedByReference = false;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    public function makeOptional(): void
    {
        $this->optional = true;
    }

    /**
     * @return boolean
     */
    public function isPassedByReference(): bool
    {
        return $this->passedByReference;
    }

    public function makePassedByReference(): void
    {
        $this->passedByReference = true;
    }

    public function makePassedByValue(): void
    {
        $this->passedByReference = false;
    }

    public function isNullAllowed(): bool
    {
        return $this->allowNull;
    }

    public function makeNullAllowed(): void
    {
        $this->allowNull = true;
    }

    public function makeNullNotAllowed(): void
    {
        $this->allowNull = false;
    }

    /**
     * @return string
     */
    abstract public function toCode(): string;
}
