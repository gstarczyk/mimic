<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

class ClassAttributeDefinition
{
    /** @var string */
    private $visibility = 'private';

    /** @var string */
    private $name;

    /** @var  mixed */
    private $value;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->setName($name);
    }

    /**
     * @return string
     */
    public function getVisibility(): string
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     */
    public function setVisibility(string $visibility)
    {
        $this->visibility = $visibility;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function toCode(): string
    {
        return sprintf('%s $%s;', $this->visibility, $this->name);
    }
}
