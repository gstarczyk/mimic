<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Mock;

use Gstarczyk\Mimic\Mock\MethodArgument\ArgumentDefinition;

class MethodDefinition
{
    /** @var  string */
    private $methodName;

    /** @var ArgumentDefinition[] */
    private $arguments = [];

    /** @var array */
    private $instructions = [];

    /** @var string */
    private $returnType;

    /**
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }

    /**
     * @param string $methodName
     */
    public function setMethodName(string $methodName): void
    {
        $this->methodName = $methodName;
    }

    /**
     * @return array
     */
    public function getInstructions(): iterable
    {
        return $this->instructions;
    }

    public function setInstructions(iterable $instructions): void
    {
        $this->instructions = $instructions;
    }

    public function addInstruction(string $instruction): void
    {
        $this->instructions[] = $instruction;
    }

    /**
     * @return ArgumentDefinition[]
     */
    public function getArguments(): iterable
    {
        return $this->arguments;
    }

    /**
     * @param ArgumentDefinition[] $arguments
     */
    public function setArguments(iterable $arguments): void
    {
        $this->arguments = [];
        foreach ($arguments as $argument) {
            $this->addArgument($argument);
        }
    }

    /**
     * @param ArgumentDefinition $argument
     */
    public function addArgument(ArgumentDefinition $argument): void
    {
        $this->arguments[] = $argument;
    }

    /**
     * @return string
     */
    public function toCode(): string
    {
        $arguments = [];
        foreach ($this->arguments as $argument) {
            $arguments[] = $argument->toCode();
        }

        $returnType = $this->returnType !== null ? ': '.$this->returnType : '';

        $code = sprintf(
                'public function %s(%s)%s',
                $this->methodName,
                implode(', ', $arguments),
                $returnType
            ).PHP_EOL;
        $code .= '{'.PHP_EOL;
        foreach ($this->instructions as $instruction) {
            $code .= $instruction.';'.PHP_EOL;
        }
        $code .= '}';

        return $code;
    }

    public function setReturnType(string $type = null): void
    {
        $this->returnType = $type;
    }

    /**
     * @return string|null
     */
    public function getReturnType(): ?string
    {
        return $this->returnType;
    }
}
