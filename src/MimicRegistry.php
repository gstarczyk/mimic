<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\Stub\Stub;

class MimicRegistry
{
    /** @var Stub[] */
    private $stubs = [];

    /** @var InvocationRegistry[] */
    private $invocationRegistries = [];

    /** @var MockFactory */
    private $mockFactory;

    public function __construct(MockFactory $mockFactory)
    {
        $this->mockFactory = $mockFactory;
    }

    /**
     * @param string $className
     *
     * @return object
     */
    public function getMock(string $className): object
    {
        $invocationRegistry = new InvocationRegistry();
        $stub = new Stub($invocationRegistry);
        $invocationHandler = new InvocationHandler(
            $invocationRegistry, $stub
        );
        $mock = $this->mockFactory->createMock($className, $invocationHandler);
        $this->registerInvocationRegistry($mock, $invocationRegistry);
        $this->registerStub($mock, $stub);

        return $mock;
    }

    /**
     * @param object $mock
     *
     * @return Stub
     */
    public function getStub(object $mock): Stub
    {
        $className = get_class($mock);
        if (!array_key_exists($className, $this->stubs)) {
            throw new MimicException(sprintf('No stub registered for mock "%s"', $className));
        }

        return $this->stubs[$className];
    }

    /**
     * @param object $mock
     *
     * @return InvocationRegistry
     */
    public function getInvocationRegistry(object $mock): InvocationRegistry
    {
        $className = get_class($mock);
        if (!array_key_exists($className, $this->invocationRegistries)) {
            throw new MimicException(sprintf('No invocation registry registered for mock "%s"', $className));
        }

        return $this->invocationRegistries[$className];
    }

    /**
     * @param object $mock
     * @param Stub $stub
     *
     * @return void
     */
    private function registerStub(object $mock, Stub $stub): void
    {
        $className = get_class($mock);
        $this->stubs[$className] = $stub;
    }

    /**
     * @param object $mock
     * @param InvocationRegistry $invocationRegistry
     *
     * @return void
     */
    private function registerInvocationRegistry(object $mock, InvocationRegistry $invocationRegistry): void
    {
        $className = get_class($mock);
        $this->invocationRegistries[$className] = $invocationRegistry;
    }
}
