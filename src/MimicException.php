<?php

namespace Gstarczyk\Mimic;

use RuntimeException;

/**
 * Used as base for all Mimic exceptions
 * Extends RuntimeException to avoid forcing handling it (used in testing)
 */
class MimicException extends RuntimeException
{

}
