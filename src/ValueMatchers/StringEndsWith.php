<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class StringEndsWith implements ValueMatcher
{
    private $suffix;

    /**
     * @param mixed $suffix
     */
    public function __construct($suffix)
    {
        $this->suffix = $suffix;
    }

    public function match($value): bool
    {
        $suffixLength = mb_strlen($this->suffix);

        return mb_substr($value, -$suffixLength) == $this->suffix;
    }
}
