<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ValueMatchers;

use Gstarczyk\Mimic\ValueMatcher;

class AnyIntegerMatcher implements ValueMatcher
{
    public function match($value): bool
    {
        return is_int($value);
    }
}
