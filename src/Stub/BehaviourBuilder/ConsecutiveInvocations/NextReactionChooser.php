<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations;

interface NextReactionChooser
{
    /**
     * @param mixed $value
     * @return NextReactionChooser
     */
    public function thenReturn($value);

    /**
     * @param \Closure $callback
     * @return NextReactionChooser
     */
    public function thenReturnCallbackResult(\Closure $callback);

    /**
     * @param \Exception $exception
     * @return NextReactionChooser
     */
    public function thenThrow(\Exception $exception);
}
