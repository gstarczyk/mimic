<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Stub\BehaviourBuilder\ConsecutiveInvocations;

interface ReactionChooser
{
    /**
     * @param mixed $value
     * @return NextReactionChooser
     */
    public function willReturn($value);

    /**
     * @param \Closure $callback
     * @return NextReactionChooser
     */
    public function willReturnCallbackResult(\Closure $callback);

    /**
     * @param \Exception $exception
     * @return NextReactionChooser
     */
    public function willThrow(\Exception $exception);
}
