<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\Stub\Actions;

use Gstarczyk\Mimic\InvocationSignature;
use Gstarczyk\Mimic\Stub\Action;

class DoNothing implements Action
{
    public function perform(InvocationSignature $invocationSignature)
    {
        return null;
    }
}
