<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

interface FirstInvocationArguments
{
    /**
     * @param $_
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWith($_): ConsecutiveInvocationArguments;

    /**
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWithAnyArguments(): ConsecutiveInvocationArguments;

    /**
     * @return ConsecutiveInvocationArguments
     */
    public function wasCalledWithoutArguments(): ConsecutiveInvocationArguments;
}
