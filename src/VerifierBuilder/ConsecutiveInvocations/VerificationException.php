<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\VerifierBuilder\ConsecutiveInvocations;

use Gstarczyk\Mimic\MimicException;

class VerificationException extends MimicException
{

}
