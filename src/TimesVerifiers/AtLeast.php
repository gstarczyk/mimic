<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\TimesVerifiers;

class AtLeast extends AbstractVerifier
{
    public function verify(int $times): void
    {
        if (!($times >= $this->getExpectedTimes())) {
            throw new TimesVerificationException(
                sprintf(
                    'Expected at least %d, but %d was given.',
                    $this->getExpectedTimes(),
                    $times
                )
            );
        }
    }
}
