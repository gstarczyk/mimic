<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

class UniqueIdGenerator
{
    /**
     * @return string
     */
    public function generateId(): string
    {
        return uniqid();
    }
}
