<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\TimesVerifiers\TimesVerificationException;

interface TimesVerifier
{
    /**
     * @param int $times
     * @throws TimesVerificationException
     */
    public function verify(int $times): void;
}
