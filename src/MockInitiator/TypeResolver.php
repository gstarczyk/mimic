<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

class TypeResolver
{
    const SEPARATOR = '\\';

    private static $builtInTypesMap = [
        'string' => 'string',
        'int' => 'integer',
        'integer' => 'integer',
        'float' => 'float',
        'bool' => 'boolean',
        'boolean' => 'boolean',
        'array' => 'array',
        'mixed' => 'mixed',
        'resource' => 'resource',
        'callable' => 'callable',
        'callback' => 'callable',
    ];

    /**
     * @param string $docComment
     * @param Context $context
     * @return string | null
     */
    public function resolveType(string $docComment, Context $context): ?string
    {
        $type = $this->getType($docComment);

        if ($type === null) {
            return $type;
        }

        if ($this->isBuiltInType($type)) {
            return self::$builtInTypesMap[$type];
        }

        if ($this->isFullClassName($type)) {
            return $type;
        }

        foreach ($context->getImports() as $import) {
            if ($this->matchByDirectClassNameUsage($import, $type)) {
                return $import->getFullClassName();
            } elseif ($this->matchByRelativeClassNameUsage($import, $type)) {
                return $import->getNamespace().self::SEPARATOR.$type;
            }
        }

        return $context->getNamespace().self::SEPARATOR.$type;
    }

    /**
     * @param string $docComment
     * @return null|string
     */
    private function getType(string $docComment): ?string
    {
        $matches = [];
        $type = null;
        if (preg_match('/@var\s+([^\s]+)/', $docComment, $matches)) {
            $type = $matches[1];
        }

        return $type;
    }

    private function isBuiltInType(string $type): bool
    {
        return array_key_exists($type, self::$builtInTypesMap);
    }

    private function matchByDirectClassNameUsage(Import $import, string $type): bool
    {
        if ($import->hasAlias()) {
            $match = $this->matchByAlias($import, $type);
        } else {
            $match = $this->matchByShortClassName($import, $type);
        }

        return $match;
    }

    /**
     * @param Import $import
     * @param string $type
     * @return bool
     */
    private function matchByShortClassName(Import $import, string $type): bool
    {
        return ($import->getShortClassName() == $type);
    }

    private function matchByRelativeClassNameUsage(Import $import, string $type): bool
    {
        $result = false;
        $relativeParts = explode(self::SEPARATOR, $type);
        if (count($relativeParts) > 1) {
            $first = $relativeParts[0];
            $result = ($import->getShortClassName() == $first);
        }

        return $result;
    }

    /**
     * @param Import $import
     * @param string $type
     * @return bool
     */
    private function matchByAlias(Import $import, string $type): bool
    {
        return ($import->getAlias() == $type);
    }

    /**
     * @param string $type
     * @return bool
     */
    private function isFullClassName(string $type): bool
    {
        return substr($type, 0, 1) === '\\';
    }
}
