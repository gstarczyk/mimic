<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

class ImportsExtractor
{
    /** @var ImportFactory */
    private $importFactory;

    /** @var FileReader */
    private $fileReader;

    public function __construct(ImportFactory $importFactory, FileReader $fileReader)
    {
        $this->importFactory = $importFactory;
        $this->fileReader = $fileReader;
    }

    public function getImports(string $filePath): iterable
    {
        $imports = [];

        $fileContent = $this->fileReader->getContents($filePath);
        $statements = $this->extractStatements($fileContent);
        foreach ($statements as $statement) {
            $imports = array_merge($imports, $this->extractImports($statement));
        }

        return $imports;
    }

    /**
     * @param string $fileContent
     * @return string[]
     */
    private function extractStatements(string $fileContent): iterable
    {
        $matches = [];
        if (preg_match_all('/use ([^;]+);/', $fileContent, $matches)) {

            return $matches[1];
        }

        return [];
    }

    /**
     * @param string $statement
     * @return Import[]
     */
    private function extractImports(string $statement): iterable
    {
        $imports = [];
        $tokens = explode(',', $statement);
        foreach ($tokens as $token) {
            $imports[] = $this->extractImport(trim($token));
        }

        return $imports;
    }

    private function extractImport(string $token): Import
    {
        $matches = [];
        if (preg_match('/([^ ]+)( as ([^ ]+))/', $token, $matches)) {
            $import = $this->importFactory->createImport($matches[1], $matches[3]);
        } else {
            $import = $this->importFactory->createImport($token);
        }

        return $import;
    }
}
