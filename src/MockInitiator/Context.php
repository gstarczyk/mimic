<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

class Context
{
    /** @var object */
    private $parentObject;

    /** @var Import[] */
    private $imports = [];

    /** @var string */
    private $namespace;

    /**
     * @param object $parentObject
     * @param string $namespace
     * @param Import[] $imports
     */
    public function __construct(object $parentObject, string $namespace, iterable $imports)
    {
        $this->parentObject = $parentObject;
        $this->setImports($imports);
        $this->namespace = $namespace;
    }

    /**
     * @param Import[] $imports
     */
    private function setImports(iterable $imports): void
    {
        foreach ($imports as $import) {
            $this->appendImport($import);
        }
    }

    private function appendImport(Import $import): void
    {
        $this->imports[] = $import;
    }

    /**
     * @return object
     */
    public function getParentObject(): object
    {
        return $this->parentObject;
    }

    /**
     * @return Import[]
     */
    public function getImports(): iterable
    {
        return $this->imports;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }
}
