<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionProperty;

class ObjectPropertyFactory
{
    /** @var TypeResolver */
    private $typeResolver;

    public function __construct(TypeResolver $typeResolver)
    {
        $this->typeResolver = $typeResolver;
    }

    public function createObjectProperty(ReflectionProperty $property, Context $context): ObjectProperty
    {
        $type = $marker = null;
        $docComment = $property->getDocComment();
        if (is_string($docComment)) {
            $type = $this->typeResolver->resolveType($docComment, $context);
            $marker = $this->getMarker($docComment);
        }

        return new ObjectProperty(
            $property,
            $context->getParentObject(),
            $type,
            $marker
        );
    }

    private function getMarker(string $docComment): ?string
    {
        $markers = [
            ObjectProperty::MARKER_MOCK,
            ObjectProperty::MARKER_TARGET,
        ];

        foreach ($markers as $marker) {
            $pattern = '/\s'.$marker.'\s/';
            if (preg_match($pattern, $docComment)) {
                return $marker;
            }
        }

        return null;
    }
}
