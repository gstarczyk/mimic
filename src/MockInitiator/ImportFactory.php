<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

class ImportFactory
{
    /**
     * @param string $fullClassName
     * @param string | null $alias
     * @return Import
     */
    public function createImport(string $fullClassName, string $alias = null): Import
    {
        $tmp = explode('\\', $fullClassName);
        $shortClassName = array_pop($tmp);
        $namespace = '\\'.trim(implode('\\', $tmp), '\\');

        $import = new Import($namespace, $shortClassName, $alias);

        return $import;
    }
}
