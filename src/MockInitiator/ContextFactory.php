<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use Gstarczyk\Mimic\MimicException;
use ReflectionClass;
use ReflectionException;

class ContextFactory
{
    const NAMESPACE_SEPARATOR = '\\';

    /** @var ImportsExtractor */
    private $importsExtractor;

    public function __construct(ImportsExtractor $importsExtractor)
    {
        $this->importsExtractor = $importsExtractor;
    }

    public function createContext(object $parentObject): Context
    {
        $imports = $this->getImports($parentObject);
        $parts = explode(self::NAMESPACE_SEPARATOR, get_class($parentObject));
        array_pop($parts);
        $namespace = self::NAMESPACE_SEPARATOR.implode(self::NAMESPACE_SEPARATOR, $parts);

        return new Context($parentObject, $namespace, $imports);
    }

    private function getImports(object $parentObject): iterable
    {
        try {
            $class = new ReflectionClass($parentObject);
            $filename = $class->getFileName();
            if ($filename) {
                return $this->importsExtractor->getImports($filename);
            }

            return [];
        } catch (ReflectionException $e) {
            throw new MimicException('Cannot extract imports from class. '.$e->getMessage());
        }
    }
}
