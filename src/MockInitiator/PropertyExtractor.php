<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

use ReflectionObject;

class PropertyExtractor
{
    /** @var ObjectPropertyFactory */
    private $propertyFactory;

    /** @var ContextFactory */
    private $contextFactory;

    public function __construct(ObjectPropertyFactory $propertyFactory, ContextFactory $contextFactory)
    {
        $this->propertyFactory = $propertyFactory;
        $this->contextFactory = $contextFactory;
    }

    /**
     * @param object $parentObject
     *
     * @return ObjectProperty[]
     */
    public function extractProperties(object $parentObject): iterable
    {
        $class = new ReflectionObject($parentObject);
        $context = $this->contextFactory->createContext($parentObject);
        $result = [];
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $result[] = $this->propertyFactory->createObjectProperty($property, $context);
        }

        return $result;
    }
}
