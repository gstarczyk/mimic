<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\MockInitiator;

class Import
{
    /** @var string */
    private $namespace;

    /** @var string */
    private $shortClassName;

    /** @var string */
    private $alias;

    /**
     * @param string $namespace
     * @param string $shortClassName
     * @param string | null $alias
     */
    public function __construct(string $namespace, string $shortClassName, string $alias = null)
    {
        $this->namespace = $namespace;
        $this->shortClassName = $shortClassName;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getFullClassName(): string
    {
        if ($this->namespace === '\\') {
            return sprintf('\%s', $this->shortClassName);
        }

        return sprintf('%s\%s', $this->namespace, $this->shortClassName);
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getShortClassName(): string
    {
        return $this->shortClassName;
    }

    /**
     * @return string | null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @return bool
     */
    public function hasAlias(): bool
    {
        return is_string($this->alias);
    }
}
