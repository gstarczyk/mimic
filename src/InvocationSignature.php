<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

class InvocationSignature
{
    /** @var string */
    private $methodName;

    /** @var array */
    private $arguments;

    /**
     * @param string $methodName
     * @param array $arguments
     */
    public function __construct(string $methodName, array $arguments)
    {
        $this->methodName = $methodName;
        $this->arguments = $arguments;
    }

    /**
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}
