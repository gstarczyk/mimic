<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;

class EmptyArguments implements ArgumentsMatcher
{
    public function match(array $arguments): bool
    {
        return empty($arguments);
    }
}
