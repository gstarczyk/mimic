<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;

class ArgumentsCaptor implements ArgumentsMatcher
{
    private $values = [];

    public function match(array $arguments): bool
    {
        $this->values[] = $arguments;

        return true;
    }

    public function getValues()
    {
        return $this->values;
    }
}
