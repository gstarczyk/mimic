<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic\ArgumentsMatchers;

use Gstarczyk\Mimic\ArgumentsMatcher;
use Gstarczyk\Mimic\ValueMatcher;
use Gstarczyk\Mimic\ValueMatchers\EqualMatcher;
use InvalidArgumentException;

class ArgumentsMatcherFactory
{
    /**
     * @param array | null $values
     * @return ArgumentsMatcher
     */
    public function createMatcher(array $values = null): ArgumentsMatcher
    {
        if ($values === null) {
            $matcher = new AnyArguments();
        } elseif (empty($values)) {
            $matcher = new EmptyArguments();
        } else {
            $this->validateValues($values);
            if ($values[0] instanceof ArgumentsCaptor) {
                return $values[0];
            } else {
                $valueMatchers = $this->createValueMatchersCollection($values);
                $matcher = new MatchingValues($valueMatchers);
            }
        }

        return $matcher;
    }

    private function validateValues(array $values): void
    {
        $captors = array_filter(
            $values,
            function ($value) {
                return $value instanceof ArgumentsCaptor;
            }
        );

        if (count($captors) > 0 && count($values) > 1) {
            throw new InvalidArgumentException('ArgumentCaptor cannot be mixed with other matchers');
        }
    }

    /**
     * @param array $values
     * @return ValueMatcher[]
     */
    private function createValueMatchersCollection(array $values): iterable
    {
        $matchers = [];
        foreach ($values as $value) {
            if ($value instanceof ValueMatcher) {
                $matchers[] = $value;
            } else {
                $matchers[] = new EqualMatcher($value);
            }
        }

        return $matchers;
    }
}
