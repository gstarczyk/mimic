<?php
declare(strict_types=1);

namespace Gstarczyk\Mimic;

use Gstarczyk\Mimic\ArgumentsMatchers\ArgumentsMatcherFactory;
use Gstarczyk\Mimic\Mock\ClassDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodArgumentFactory;
use Gstarczyk\Mimic\Mock\MethodDefinitionFactory;
use Gstarczyk\Mimic\Mock\MethodReturnTypeFactory;
use Gstarczyk\Mimic\Mock\MockFactory;
use Gstarczyk\Mimic\MockInitiator\ContextFactory;
use Gstarczyk\Mimic\MockInitiator\FileReader;
use Gstarczyk\Mimic\MockInitiator\ImportFactory;
use Gstarczyk\Mimic\MockInitiator\ImportsExtractor;
use Gstarczyk\Mimic\MockInitiator\MethodArgumentsResolver;
use Gstarczyk\Mimic\MockInitiator\MockInitiator;
use Gstarczyk\Mimic\MockInitiator\ObjectPropertyFactory;
use Gstarczyk\Mimic\MockInitiator\PropertyExtractor;
use Gstarczyk\Mimic\MockInitiator\TargetObjectFactory;
use Gstarczyk\Mimic\MockInitiator\TypeResolver;
use Gstarczyk\Mimic\Stub\BehaviourBuilder\BehaviourBuilderSelector;
use Gstarczyk\Mimic\VerifierBuilder\VerifierBuilderSelector;
use InvalidArgumentException;
use ReflectionObject;

class Mimic
{
    static private $registry;

    /**
     * @return MimicRegistry
     */
    static private function getRegistry(): MimicRegistry
    {
        if (!isset(self::$registry)) {
            $mockFactory = new MockFactory(
                new ClassDefinitionFactory(
                    new UniqueIdGenerator(),
                    new MethodDefinitionFactory(
                        new MethodArgumentFactory(),
                        new MethodReturnTypeFactory()
                    )
                )
            );
            self::$registry = new MimicRegistry($mockFactory);
        }

        return self::$registry;
    }

    /**
     * @param string $className
     * @return object
     */
    static public function mock(string $className): object
    {
        return self::getRegistry()->getMock($className);
    }

    /**
     * @param object $mock
     * @return BehaviourBuilderSelector
     */
    static public function when(object $mock): BehaviourBuilderSelector
    {
        return new BehaviourBuilderSelector(self::getRegistry()->getStub($mock));
    }

    /**
     * @param object $mock
     * @param string $method
     * @return void
     */
    static public function spy(object $mock, string $method): void
    {
        $behavior = new BehaviourBuilderSelector(self::getRegistry()->getStub($mock));
        $reflection = new ReflectionObject($mock);
        $parentClass = $reflection->getParentClass();

        if ($parentClass == null) {
            throw new MimicException(
                sprintf(
                    'Cannot spy an interface [interfaceName=%s]',
                    implode(', ', $reflection->getInterfaceNames())
                )
            );
        }

        if (!is_callable([$mock, 'parent::'.$method])) {
            throw new MimicException(
                sprintf(
                    'Cannot spy an abstract or invalid method [methodName=%s::%s]',
                    $parentClass->getName(),
                    $method
                )
            );
        }

        $behavior->invoke($method)->withAnyArguments()->willReturnCallbackResult(
            function () use ($mock, $method) {
                return call_user_func_array([$mock, 'parent::'.$method], func_get_args());
            }
        );
    }

    /**
     * @param object $mock
     * @return VerifierBuilderSelector
     */
    static public function verify(object $mock): VerifierBuilderSelector
    {
        return new VerifierBuilderSelector(
            self::getRegistry()->getInvocationRegistry($mock),
            new ArgumentsMatcherFactory()
        );
    }

    /**
     * @param $mock
     * @param $_ $arg1, $arg2, ...
     */
    static public function callConstructor(object $mock, $_): void
    {
        $reflection = new ReflectionObject($mock);
        $parentClass = $reflection->getParentClass();

        if ($parentClass === null) {
            throw new InvalidArgumentException(
                sprintf(
                    'Cannot call constructor of an interface [interfaceName=%s]',
                    implode(', ', $reflection->getInterfaceNames())
                )
            );
        }

        $parent = $parentClass->getName();

        if ($parentClass->getConstructor()->isAbstract()) {
            throw new InvalidArgumentException(sprintf('Cannot call an abstract constructor [class=%s]', $parent));
        }

        $constructorArgs = func_get_args();
        array_shift($constructorArgs);
        $parentClass->getConstructor()->invokeArgs($mock, $constructorArgs);
    }

    /**
     * Automatically create mocks for annotated test case properties
     * @param object $testCase
     */
    static public function initMocks(object $testCase): void
    {
        $initiator = new MockInitiator(
            self::getRegistry(),
            new PropertyExtractor(
                new ObjectPropertyFactory(
                    new TypeResolver()
                ),
                new ContextFactory(
                    new ImportsExtractor(
                        new ImportFactory(),
                        new FileReader()
                    )
                )
            ),
            new TargetObjectFactory(
                new MethodArgumentsResolver()
            )
        );

        $initiator->initMocks($testCase);
    }
}
